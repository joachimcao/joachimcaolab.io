---
title: "Curriculum Vitæ"
description: "Curriculum Vitæ"
hideBackToTop: false
hidePagination: true
---


[*download pdf*](/cv/cv-haicao.pdf)

#### Education




#### Research Interest

- Computer Architecture
- RISC-V Processors
- Application-Oriented Processors
- Hardware Accelerator.


#### Work Experience

#### Publications

Please refer to [Publications](/pub)

#### Technical Skills


#### Languages

1. **English** – C1
2. **French** – A1
3. **Latin**