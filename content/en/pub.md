---
title: "Publications"
description: "Books I liked."
hideBackToTop: false
hidePagination: true
---

#### First-Author


#### Collaborative

N. M. Dang, H. X. Cao, and L. Tran \
[Batage-bfnp: A high-performance hybrid branch predictor with data-dependent branches speculative pre-execution for risc-v processors](https://link.springer.com/article/10.1007/s13369-022-07593-9)

