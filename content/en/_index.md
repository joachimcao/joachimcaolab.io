---
title: "Home"
description: "Home"
---

# Hello,

I am a Computer Engineer with a strong background in electronics engineering and a passion for computer architecture.
My work focuses on designing and optimizing processors, particularly using RISC-V architecture.

Feel free to learn more [about me](/about) or explore my [projects](/proj).
