---
title: "About"
description: "A description of Francesco Tomaselli, an Italian software engineer."
hideBackToTop: true
hidePagination: true
---

I am passionate about computer architecture, with a strong focus on designing and optimizing processors.
I got both my Bachelor's and Master's degree in Electronics Engineering from HCMC University of Technology, VNU, where I now work as a lecturer.

My academic journey has centered on designing RISC-V processors, with my theses exploring performance enhancements through techniques such as superscalar execution, out-of-order processing, and branch prediction.

In my free time, I enjoy practicing calligraphy and immersing myself in reading.
You can explore more of my work and thoughts on my personal blog at [thejoachim.gitlab.io](https://thejoachim.gitlab.io).